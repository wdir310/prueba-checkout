<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/themes/default.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/alertify.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Roboto:700&display=swap" rel="stylesheet">
	</head>

	<body>
		<div>
			<img src="bootstrap/img/bg-2.png" class="img-fluid">
		</div>
		<div class="container">
				<?php  
				session_start(); 
				$data = $_SESSION['url_content'];
				$obj = json_decode($data, true);
				?>
			<div class="card">
			    <form method="POST" action="">
				<div class="card-body">
			    	<h5 class="text-center">COMPRA: <?php echo strtoupper($obj['name']); ?></h5><br>
			    	<div class="row justify-content-center">
					   <div class="col-sm-3"><label>Total producto: </label></div>
					   <div class="col-sm-3" align="right"><span> $ <?php echo number_format($obj['price']); ?></span></div>
					</div>
					<div class="row justify-content-center">
						<div class="col-6">
						<hr size=4>
						</div>
					</div>
					<div class="row justify-content-center">
					    <div class="col-3">
					     <label>Detalles de la compra: </label>
					    </div>
					    <div class="col-3" align="right">
					      <span>$ <?php echo number_format($obj['price']); ?></span> 
					    </div>

					</div>
					<?php  if($obj['id'] == 1){ $Total = $obj['price'] + $obj['tax']; ?>
				    <div class="row justify-content-center">
				    	<div class="col-3">
				    	</div>
					    <div class="col-3" align="right">
					      <label>+<?php echo number_format($obj['tax']); ?> imp.</label> 
					    </div>
				    </div>
					<?php } ?>
				    <?php $desc=20; if($obj['id'] == 1 and date("G")%2 == 0){ 
				    	$price_tax = $obj['price'] + $obj['tax'];
						$result = $price_tax * 0.80;
						$Total = round($result,2);
				    ?>
				    <div class="row justify-content-center">
					    <div class="col-3">
				    	</div>
					    <div class="col-3" align="right">
					      <h3>-<?php echo $desc; ?>% de descuento</h3> 
					    </div>
				    </div>
				    <?php } ?>
				    <?php  if($obj['id'] != 1){ $Total = $obj['price'];} ?>
			  		<div class="row justify-content-center">
						<div class="col-6">
						<hr size=4>
						</div>
					</div>
					<div class="row justify-content-center">
					    <div class="col-3">
					     <label>Total de la compra: </label>
					    </div>
					    <div class="col-3" align="right">
					     <span> $ <?php echo number_format($Total); ?> </span>
					    </div>
					</div>
	  				<br>
				    <div class="row justify-content-center">
					    <div class="col-3">
					      <label><i class="fas fa-user"></i>  Nombre</label>
					      <input type="text" name="first_name" class="form-control"  pattern=".{3,}"title="Nombre ( min . 3 caracteres)" onkeypress="return SoloLetras(event)" required="">
					    </div>
					    <div class="col-3">
					      <label> Apellido</label>
					      <input type="text" name="last_name" class="form-control" pattern=".{3,}" title="Apellido ( min . 3 Letras)" onkeypress="return SoloLetras(event)" required="">
					    </div>
				  	</div>
				  	<br>
				  	<div class="row justify-content-around">
				  		<div class="col-6">
					    <label><i class="fas fa-envelope"></i>  E-mail</label>
					    <input type="email" name="email" class="form-control" required="">
					    </div>
				  	</div>
				  	<br>
				  	<div class="row justify-content-around">
				  		<div class="col-6">
					    <label><i class="far fa-credit-card"></i>  Tarjeta</label>
					    <input type="text" name="tarjet" maxlength="15" class="form-control" onkeypress="return SoloNumeros(event)" pattern=".{15,}" required="">
				  		</div>
				  	</div>
				  	<br>
				  	<div class="row justify-content-around">
				  		<div class="col-6">
					   <button id="alerti" type="submit" name="send" class="btn btn-secondary" >Finalizar compra</button>
				  		</div>
				  	</div>
				</div>
				</form>
			<?php include 'Api_key.php'; ?>
			</div>
			<br>
			<br>
			<div class="text-center">
				<img src="bootstrap/img/logo.png" width="12%" class="img-fluid">
			</div>
		</div>

		<script src="bootstrap/js/jquery.js"></script>
		<script src="bootstrap/js/all.js"></script>
		<script src="bootstrap/js/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="bootstrap/js/alertify.js"></script>

		<script type="text/javascript">

		function SoloLetras(e) {
			tecla = (document.all) ? e.keyCode : e.which;
			if (tecla==8) return true;
				patron =/[A-Za-z\s]/;
				te = String.fromCharCode(tecla);
			return patron.test(te);
		}

		function SoloNumeros(e) {
			var key = window.Event ? e.which : e.keyCode
			return ((key >= 48 && key <= 57) || (key==8))
		}


		$(document).ready(function(){
			$('#alerti').click(function(){
				alertify.success("¡Compra exitosa!")
			})

		});


		</script>

	</body>
</html>