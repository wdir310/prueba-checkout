<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Roboto:700&display=swap" rel="stylesheet">
	</head>

	<body>
		<div><img src="bootstrap/img/bg-1.png" class="img-fluid"></div>
		<form action="Buy.php" method="post">
		<div class="container">
			<?php  
				session_start();
				$url="http://sigmatest.sigmastorage.online/"; // url de datos
				$_SESSION['url_content'] = '';  
				$file = @fopen($url, 'r');  //Arroja string
				if($file){  
				  while(!feof($file)) {  
				    $_SESSION['url_content'] .= @fgets($file, 4096);
				   	$obj = json_decode($_SESSION['url_content'], true);//formateo a json
			?> 
			<div class="card" >
			    <div class="row no-gutters">
				   <div class="col-md-4"><img src="<?php echo $obj['image']; ?>" class="card-img" width="1px"></div>
				   	<div class="col-md-8">
					    <div class="card-body">
					        <h5><?php echo strtoupper($obj['name']); ?></h5>
					        <div class="form-group">
				  				<span>$ <?php echo number_format($obj['price']); ?></span><br>
				  				<?php $desc=20; if($obj['id'] == 1){ ?>
				  				<label>$ <?php echo number_format($obj['tax']); ?> imp.</label>
				  				<?php  if(date("G")%2 == 0){ ?><br>
				  				<h3>-<?php echo $desc; ?>%</h3>
				  				<?php } } ?>								
				  				<p> Descripción</p>
				  				<hr size=5>
	   						</div>
						   	<?php
									}  
								  	fclose ($file);  
								}  
							?>  
					        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					        consequat.</p>
					        <div class="form-group">
				  				<button type="submit" class="btn btn-secondary btn-sm" >Comprar</button>
				  			</div>
				  			</form>
				  		</div>
				  	</div>		
			    </div>
			</div>
			<br>
			<br>
		<div class="text-center">
			<img src="bootstrap/img/logo.png" width="12%" class="img-fluid">
		</div>
		</div>

		<script src="bootstrap/js/jquery.js"></script>
		<script src="bootstrap/js/all.js"></script>
		<script src="bootstrap/js/jquery.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>